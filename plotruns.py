#! /usr/bin/env python3.9

import sys
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt


def parse_scores(scores):
    scores = scores.strip('{}').split(',')
    scores = dict(tuple(map(int, score.split(':'))) for score in scores)
    return scores


def readfile(filename, nsplit=None):
    records = []
    total = defaultdict(int)
    totals = defaultdict(list)
    nparts = 0
    with open(filename) as fp:
        for i, line in enumerate(fp):
            record = {}
            parts = line.split(';')
            for part in parts:
                key, value = part.split('=')
                record[key] = value
            game = record['game'].split('-')[0].strip().lower()
            scores = parse_scores(record['scores'])
            for key, value in scores.items():
                total[key] += value
            if nsplit and (i+1) % nsplit == 0:
                for key, value in total.items():
                    totals[key].append(value)
                    total[key] = 0
                nparts += 1
            records.append(record)
    if not nsplit:
        totals = {key: [value] for key, value in total.items()}
        nparts = 1
    return totals, len(records) // nparts, game


def plot(scores, name, binsize):
    #fig, ax = plt.subplots()
    offset = 0
    players = sorted(scores.keys())
    for i, player in enumerate(players):
        data = scores[player]
        x = np.arange(len(data))
        #data = [scores[player] for player in players]
        plt.bar(x+offset, data, 0.2)
        offset += 0.2
    #plt.hist(data, bins=binsize, rwidth=0.7, align="left")
    ylim = plt.ylim()
    if ylim[0] < 0:
        plt.ylim(plt.ylim()[::-1])
    names = {1: 'A', 2: 'B', 3: 'C', 4: 'D'}
    #plt.legend([names[name] for name in players])
    plt.ylabel("Score")
    plt.title(name.capitalize())
    plt.gca().axes.xaxis.set_visible(False)
    #plt.show()

nsplit = 4000
plt.figure(figsize=(12, 16))
for i, arg in enumerate(sys.argv[1:]):
    plt.subplot(4, 3, i+1)
    #plt.subplot(111)
    averages = {}
    scores, n, name = readfile(arg, nsplit=nsplit)

    for key, value in scores.items():
        averages[key] = [score/n for score in value]
    print(averages)
    if not nsplit:
        plot(averages, name, n)
    else:
        plot(averages, name, n//nsplit)
plt.tight_layout()
plt.show()
