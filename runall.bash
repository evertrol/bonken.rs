#! /bin/bash

start=$(date -Iseconds)
NSIM=${1:-10000}

cargo build --release

# Perform the simulations in parallel
for game in notrump trumpofspades trumpofhearts trumpofdiamonds trumpofclubs hearts kingofhearts kingsjacks queens seventhirteen lasttrick duck
do
		#cargo run --release --bin bonken -- $game --nsim $NSIM --output ${game}-out.txt > /dev/null &
		nice ./target/release/simweights $game --nsim $NSIM --output-games ${game}-out.dat --output-nth-game 1000 > /dev/null &
done
wait

stop=$(date -Iseconds)
echo -e "Started @ ${start}.\nAll done @ ${stop}" > runall.result
