use crate::card::{Card, Deck, Play, Rank, Suit, Trick};
use std::fmt;
pub mod duck;
pub mod hearts;
pub mod jacksandkings;
pub mod kingofhearts;
pub mod last;
pub mod notrump;
pub mod queens;
pub mod seventhirteen;
pub mod trump;

#[derive(Debug, Clone, Default)]
pub struct Info {
    // Cards in the current deck
    pub deck: Deck,
    // Cards in the trash pile
    pub pile: Deck,
}

pub trait Game {
    fn name(&self) -> String {
        "".to_string()
    }

    fn short_name(&self) -> String {
        "".to_string()
    }

    fn start(&self) {}

    fn reset(&mut self) {}

    fn deck(&self) -> Deck {
        Deck::default()
    }

    fn maxpoints(&self) -> i16 {
        0
    }

    fn calc_winner(&self, trick: &Trick) -> Option<usize> {
        if trick.plays.is_empty() {
            Some(0)
        } else {
            None
        }
    }

    fn lead_from_first(&self) -> bool {
        true
    }
    fn valid_cards_indices(&self, cards: &[Card], _trick: &Trick) -> Vec<usize> {
        (0..cards.len()).collect::<Vec<usize>>()
    }

    fn is_valid(&self, _card: &Card, _cards: &[Card], _trick: &Trick) -> bool {
        true
    }

    fn trump(&self) -> Option<Suit> {
        None
    }

    fn valid_cards(&self, cards: &[Card], _trick: &Trick) -> Vec<Card> {
        let mut allowed: Vec<Card> = vec![];
        for card in cards.iter() {
            allowed.push(*card);
        }
        allowed
    }

    // For early endings
    fn game_ends(&self, _plays: &[Vec<Play>]) -> bool {
        false
    }

    fn score(&self, _player: usize, _winner: usize, _trick: &Trick, _round: usize) -> i16 {
        0
    }
}

impl fmt::Debug for dyn Game {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("game trait")
    }
}

#[derive(Debug, Clone, Default)]
pub struct Default {
    name: String,
    pub ranking: [Rank; 13],
}

impl Game for Default {
    fn name(&self) -> String {
        self.name.clone()
    }
}
