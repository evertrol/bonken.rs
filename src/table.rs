use crate::card::{Deck, Play, Trick};
//use crate::game::Default as DefaultGame;
use crate::game::{self, Game};
use crate::player::{Player, PlayerId};
use rand::prelude::SliceRandom;
use rand::rngs::ThreadRng;
use serde::ser; //::{self, SerializeStruct};
use serde::Serialize;
use std::collections::hash_map::HashMap;
use std::fmt;

#[derive(Debug, Clone, Default)]
pub struct Score(i16);

impl fmt::Display for Score {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl ser::Serialize for Score {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: ser::Serializer,
    {
        serializer.serialize_str(format!("{}", self).as_ref())
    }
}

#[derive(Debug, Clone, Default, Serialize)]
pub struct Round<'a> {
    pub winner: Option<Player<'a>>,
    pub plays: Vec<(Player<'a>, Play)>,
    pub scores: HashMap<PlayerId, i16>,
}

impl fmt::Display for Round<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let strings: Vec<String> = self
            .plays
            .iter()
            .map(|play| format!("{}: {}", play.0.name, play.1))
            .collect();
        if let Some(winner) = &self.winner {
            write!(f, "{}; {} wins", strings.join(", "), winner.name)
        } else {
            write!(f, "{}; no one wins", strings.join(", "))
        }
    }
}

impl<'a> Round<'a> {
    pub fn lead(&self) -> Option<&(Player<'a>, Play)> {
        if self.plays.is_empty() {
            None
        } else {
            Some(&self.plays[0])
        }
    }
}

#[derive(Debug, Serialize)]
pub struct Table<'a> {
    #[serde(skip)]
    pub deck: Deck,
    #[serde(skip)]
    pub trash_pile: Deck,
    pub players: Vec<Player<'a>>,
    pub rounds: Vec<Round<'a>>,
    #[serde(skip)]
    pub game: Box<dyn Game>,
    pub scores: HashMap<PlayerId, i16>,
}

impl Default for Table<'_> {
    fn default() -> Self {
        //        let game: dyn Game = Box::game::Default::default();
        let deck: Deck = Default::default();
        Table {
            players: vec![],
            game: Box::new(game::Default::default()),
            rounds: vec![],
            deck,
            trash_pile: Deck::empty(),
            scores: HashMap::new(),
        }
    }
}

impl fmt::Display for Table<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Players:").unwrap();
        for player in self.players.iter() {
            writeln!(f, "{}", player).unwrap();
        }
        writeln!(f, "Deck: {}", self.deck).unwrap();
        writeln!(f, "Rounds:").unwrap();
        for round in self.rounds.iter() {
            writeln!(f, "{}", round).unwrap();
        }
        write!(f, "Final scores: ").unwrap();

        let strings: Vec<String> = self
            .players
            .iter()
            .map(|player| {
                format!(
                    "{}: {}",
                    player.name,
                    self.scores.get(&player.id).unwrap_or(&0)
                )
            })
            .collect();
        write!(f, "{}", strings.join(", "))
    }
}

impl<'a> Table<'a> {
    pub fn new() -> Self {
        Table {
            deck: Default::default(),
            trash_pile: Deck::empty(),
            players: Default::default(),
            game: Box::new(game::Default::default()),
            rounds: Default::default(),
            scores: HashMap::new(),
        }
    }

    pub fn players(mut self, players: Vec<Player<'a>>) -> Self {
        self.players = players.to_vec();
        for (i, player) in self.players.iter_mut().enumerate() {
            if player.id == 0 {
                player.id = i + 1;
            }
            self.scores.insert(player.id, 0);
        }
        self
    }

    pub fn game(mut self, game: Box<dyn Game>) -> Self {
        self.game = game;
        self.deck = self.game.deck();
        self
    }

    pub fn reset(&mut self) {
        self.deck = self.game.deck();
        for player in self.players.iter() {
            self.scores.insert(player.id, 0);
        }
    }
    // Overwrite the deck determined by the game() builder function
    pub fn deck(mut self, deck: Deck) -> Self {
        self.deck = deck;
        self
    }

    pub fn shuffle(&mut self, rng: &mut ThreadRng) {
        self.deck.shuffle(rng);
        for player in self.players.iter_mut() {
            player.reset()
        }
        self.game.reset();
    }

    pub fn deal(&mut self) {
        //let ncards = self.game.deck().len() / self.players.len();
        // TODO: use self.game_info.deck.cards.chunks?
        //for player in self.players.iter_mut() {
        //    player.hand = Default::default();
        //}
        let n = self.deck.len() / self.players.len();
        for player in self.players.iter_mut() {
            player.hand.cards = self.deck.cards.drain(..n).collect();
            //player.hand.sort();
        }
    }

    // Return a list of valid cards to play
    // These are all available cards; not just those in the players hand!
    // They should be "and"-ed with the cards in the player hand to
    // select a valid card to play.
    //pub fn valid_cards(self) {
    //    let cards = &self.game.deck().cards;
    //    let round = &self.rounds.last();
    //    let leading = round.and(match &round.unwrap().plays.first() {
    //        Some(play) => match &play.1 {
    //            Play::Card(card) => Some(card),
    //            Play::Cards(cards) => cards.first(),
    //            _ => None,
    //        },
    //        _ => None,
    //    });
    //    let mut valid: Vec<&Card> = vec![];
    //    for card in cards {
    //        if leading.is_none() || card.suit == leading.unwrap().suit {
    //            valid.push(card);
    //        }
    //    }
    //}
    //
    //pub fn is_valid_card(&self, card: &Card) -> bool {
    //    let round = &self.rounds.last();
    //    let leading = round.and(match &round.unwrap().plays.first() {
    //        Some(play) => match &play.1 {
    //            Play::Card(card) => Some(card),
    //            Play::Cards(cards) => cards.first(),
    //            _ => None,
    //        },
    //        _ => None,
    //    });
    //
    //    leading.is_none() || card.suit == leading.unwrap().suit
    //}

    pub fn finish_round(
        &mut self,
        mut round: Round<'a>,
        trick: &Trick,
        prevwinner: usize,
    ) -> usize {
        let nplayers = self.players.len();
        let mut winner = prevwinner;
        round.winner = if let Some(i) = self.game.calc_winner(trick) {
            winner = (prevwinner + i) % nplayers;
            Some(self.players[winner].clone())
        } else {
            None
        };
        for iplayer in 0..nplayers {
            let score = self.game.score(iplayer, winner, trick, self.rounds.len());
            round.scores.insert(self.players[iplayer].id, score);
            self.scores.insert(
                self.players[iplayer].id,
                self.scores.get(&self.players[iplayer].id).unwrap() + score,
            );
        }
        self.rounds.push(round);

        winner
    }

    pub fn game_ends(&self) -> bool {
        let plays: Vec<Vec<Play>> = self
            .rounds
            .iter()
            .map(|round| {
                round
                    .plays
                    .iter()
                    .map(|(_, play)| play.clone())
                    .collect::<Vec<Play>>()
            })
            .collect();
        self.game.game_ends(&plays)
    }

    pub fn play(&mut self, rng: &mut ThreadRng) {
        self.reset();
        let nrounds = self.deck.len() / self.players.len();
        self.shuffle(rng);
        self.players.shuffle(rng);
        self.deal();

        log::debug!("{}", self.game.name());
        self.rounds.truncate(0);
        for player in self.players.iter() {
            log::debug!("{}", player);
        }

        //let game = &self.game;
        let mut iwinner = 0;
        let nplayers = self.players.len();
        for iround in 0..nrounds {
            log::info!("Round {}", iround);
            if let Some(suit) = self.game.trump() {
                log::debug!(" ({})", suit);
            }
            for player in self.players.iter() {
                log::debug!("{}", player);
            }

            let mut trick = Trick::default();
            let mut round = Round::default();
            let player = &mut self.players[iwinner];
            let play = player.play(self.game.as_ref(), &trick, &self.rounds, rng);
            trick.lead = if self.game.lead_from_first() {
                match &play {
                    Play::NA | Play::Pass => None,
                    Play::Card(card) => Some(*card),
                    Play::Cards(cards) => {
                        if cards.is_empty() {
                            Some(cards[0])
                        } else {
                            None
                        }
                    }
                }
            } else {
                None
            };
            if let Some(card) = trick.lead {
                log::debug!("Lead = {}", card);
            }

            log::debug!("{} played {}", player.name, play);
            trick.add(play.clone());
            round.plays.push((player.clone(), play));
            for i in (iwinner..).take(nplayers).skip(1) {
                let player = &mut self.players[i % nplayers];
                let play = player.play(self.game.as_ref(), &trick, &self.rounds, rng);
                log::debug!("{} played {}", player.name, play);
                trick.add(play.clone());
                round.plays.push((player.clone(), play));
            }
            iwinner = self.finish_round(round, &trick, iwinner);
            log::debug!("Round played: {}", self.rounds.last().unwrap());
            if self.game_ends() {
                break;
            }
        }
        log::debug!("Overview of this game of {}:", self.game.name());
        for round in self.rounds.iter() {
            log::debug!("{}", round);
        }
        log::debug!("Scores: {:?}", self.scores);
    }
}

//    pub fn play(self, player: &Player, card: &Card) { }
