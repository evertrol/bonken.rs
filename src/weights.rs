use crate::card::{Card, Rank, Suit};
use crate::player::Turn;
use std::cmp::Ordering;
use std::collections::hash_map::HashMap;
use std::error::Error;
use std::fs::File;
use std::io;

// Complicated hashmap: store weights by round, leading suit, leading
// rank, position (turn) of the player (first, last, other) and card
// played.
pub type JSONWeights =
    HashMap<usize, HashMap<Suit, HashMap<Rank, HashMap<Turn, HashMap<Card, i64>>>>>;
pub type JSONWeights2 =
    HashMap<usize, HashMap<Suit, HashMap<Turn, HashMap<Suit, HashMap<i8, i64>>>>>;
pub type Weights = HashMap<(usize, Suit, Rank, Turn, Card), i64>;
pub type Weights2 = HashMap<(usize, Suit, Turn, Suit, i8), i64>;

pub fn read_weights(name: &str) -> Result<Weights, Box<dyn Error>> {
    let filename = format!("{}-weights.json", name);
    let file = File::open(&filename)?;
    let reader = io::BufReader::new(file);
    let weights: JSONWeights = serde_json::from_reader(reader)?;
    let weights = jsonweights_to_weights(&weights);
    Ok(weights)
}

pub fn read_weights2(name: &str) -> Result<Weights2, Box<dyn Error>> {
    let filename = format!("{}-weights2.json", name);
    let file = File::open(&filename)?;
    let reader = io::BufReader::new(file);
    let weights: JSONWeights2 = serde_json::from_reader(reader)?;
    let weights = jsonweights2_to_weights2(&weights);
    Ok(weights)
}

pub fn jsonweights_to_weights(json_weights: &JSONWeights) -> Weights {
    let mut weights: Weights = HashMap::new();
    for (iround, values1) in json_weights.iter() {
        for (suit, values2) in values1.iter() {
            for (rank, values3) in values2.iter() {
                for (turn, values4) in values3.iter() {
                    for (card, weight) in values4.iter() {
                        let key = (*iround, *suit, *rank, *turn, *card);
                        weights.insert(key, *weight as i64);
                    }
                }
            }
        }
    }
    weights
}

pub fn jsonweights2_to_weights2(json_weights: &JSONWeights2) -> Weights2 {
    let mut weights: Weights2 = HashMap::new();
    for (iround, values1) in json_weights.iter() {
        for (leadsuit, values2) in values1.iter() {
            for (turn, values3) in values2.iter() {
                for (suit, values4) in values3.iter() {
                    for (rel, weight) in values4.iter() {
                        let key = (*iround, *leadsuit, *turn, *suit, *rel);
                        weights.insert(key, *weight as i64);
                    }
                }
            }
        }
    }
    weights
}

pub fn weights2_to_jsonweights2(weights: &Weights2) -> JSONWeights2 {
    let mut json_weights: JSONWeights2 = HashMap::new();
    for (key, weight) in weights.iter() {
        let (iround, leadsuit, turn, suit, rel) = key;
        json_weights
            .entry(*iround)
            .or_insert_with(HashMap::new)
            .entry(*leadsuit)
            .or_insert_with(HashMap::new)
            .entry(*turn)
            .or_insert_with(HashMap::new)
            .entry(*suit)
            .or_insert_with(HashMap::new)
            .insert(*rel, *weight);
    }
    json_weights
}

pub fn normalize_weights(weights: &mut JSONWeights) {
    let mut min = 0;
    let mut max = 0;
    for (_, round) in weights.iter() {
        for (_, suit) in round.iter() {
            for (_, rank) in suit.iter() {
                for (_, turn) in rank.iter() {
                    for (_, weight) in turn.iter() {
                        if weight > &max {
                            max = *weight;
                        }
                        if weight < &min {
                            min = *weight;
                        }
                    }
                }
            }
        }
    }

    let min = match min.cmp(&0) {
        Ordering::Greater => (min as f64),//.ln(),
        Ordering::Less => -(-min as f64),//.ln(),
        Ordering::Equal => 0.0,
    };
    let max = match max.cmp(&0) {
        Ordering::Greater => (max as f64),//.ln(),
        Ordering::Less => -(-max as f64),//.ln(),
        Ordering::Equal => 0.0,
    };
    let delta = max - min;
    for (_, round) in weights.iter_mut() {
        for (_, suit) in round.iter_mut() {
            for (_, rank) in suit.iter_mut() {
                for (_, turn) in rank.iter_mut() {
                    for (_, weight) in turn.iter_mut() {
                        let lnweight = match (*weight).cmp(&0) {
                            Ordering::Greater => (*weight as f64),//.ln(),
                            Ordering::Less => -(-*weight as f64),//.ln(),
                            Ordering::Equal => 0.0,
                        } - min;
                        *weight = (100.0 * (lnweight as f64 / delta as f64)) as i64;
                        // Always allow for a (bad) move
                        if *weight == 0 {
                            *weight = 1;
                        }
                    }
                }
            }
        }
    }
}

pub fn normalize_weights2(weights: &mut Weights2) {
    let mut min = 0;
    let mut max = 0;
    for (_, weight) in weights.iter() {
        if weight > &max {
            max = *weight;
        }
        if weight < &min {
            min = *weight;
        }
    }

    let min = match min.cmp(&0) {
        Ordering::Greater => (min as f64),//.ln(),
        Ordering::Less => -(-min as f64),//.ln(),
        Ordering::Equal => 0.0,
    };
    let max = match max.cmp(&0) {
        Ordering::Greater => (max as f64),//.ln(),
        Ordering::Less => -(-max as f64),//.ln(),
        Ordering::Equal => 0.0,
    };
    let delta = max - min;
    for (_, weight) in weights.iter_mut() {
        let lnweight = match (*weight).cmp(&0) {
            Ordering::Greater => (*weight as f64),//.ln(),
            Ordering::Less => -(-*weight as f64),//.ln(),
            Ordering::Equal => 0.0,
        } - min;
        *weight = (100.0 * (lnweight as f64 / delta as f64)) as i64;
        // Always allow for a (bad) move
        if *weight == 0 {
            *weight = 1;
        }
    }
}
