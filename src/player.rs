use crate::card;
use crate::game::Game;
use crate::table::Round;
use crate::weights::Weights2;
use rand::distributions::WeightedIndex;
use rand::prelude::*;
use rand::rngs::ThreadRng;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::str::FromStr;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ParseError {
    InvalidTurn,
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Turn {
    First,
    Last,
    Other,
}

impl fmt::Display for Turn {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Turn::First => write!(f, "first"),
            Turn::Last => write!(f, "last"),
            Turn::Other => write!(f, "other"),
        }
    }
}

impl FromStr for Turn {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "first" => Ok(Self::First),
            "last" => Ok(Self::First),
            "other" => Ok(Self::First),
            _ => Err(ParseError::InvalidTurn),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize)]
pub enum Weighting {
    Equal,
    Simple,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize)]
pub enum Method {
    Random,
    WeightedRandom,
    Weighted,
}

impl Default for Method {
    fn default() -> Self {
        Method::Random
    }
}

#[derive(Debug, Clone, Default, PartialEq, Eq, Hash, Serialize)]
pub struct Hand {
    pub cards: Vec<card::Card>,
}

impl Hand {
    pub fn sort(&mut self) {
        self.cards.sort();
    }

    pub fn shuffle(&mut self, rng: &mut ThreadRng) {
        //let mut rng = thread_rng();
        self.cards.shuffle(rng);
    }

    pub fn len(&self) -> usize {
        self.cards.len()
    }

    pub fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }
}

impl fmt::Display for Hand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let strings: Vec<String> = self.cards.iter().map(|card| format!("{}", card)).collect();
        write!(f, "[{}]", strings.join(", "))
    }
}

pub type PlayerId = usize;

#[derive(Debug, Clone, Eq, Serialize)]
pub struct Player<'a> {
    pub id: PlayerId,
    pub name: String,
    pub hand: Hand,
    pub method: Method,
    pub weights: Option<&'a Weights2>,
}

impl PartialEq for Player<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
            && self.name == other.name
            && self.hand == other.hand
            && self.method == other.method
    }
}
impl Hash for Player<'_> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
        self.name.hash(state);
        self.method.hash(state);
        self.hand.hash(state);
    }
}

impl<'a> Player<'a> {
    pub fn new(name: &str) -> Player {
        Player {
            name: name.to_string(),
            id: 0,
            method: Method::Random,
            hand: Hand { cards: Vec::new() },
            weights: None,
        }
    }

    pub fn id(mut self, id: usize) -> Self {
        self.id = id;
        self
    }

    pub fn reset(&mut self) {
        self.hand.cards.truncate(0);
    }

    pub fn set_weights(&mut self, weights: &'a Weights2) {
        self.weights = Some(weights);
    }

    pub fn play(
        &mut self,
        game: &dyn Game,
        trick: &card::Trick,
        rounds: &[Round],
        rng: &mut ThreadRng,
    ) -> card::Play {
        match self.method {
            Method::Random => self.play_random(game, trick, rounds, rng),
            Method::WeightedRandom => self.play_weighted_random(game, trick, rounds, rng),
            Method::Weighted => self.play_weighted_random(game, trick, rounds, rng),
        }
    }

    pub fn play_random(
        &mut self,
        game: &dyn Game,
        trick: &card::Trick,
        _rounds: &[Round],
        rng: &mut ThreadRng,
    ) -> card::Play {
        let mut allowed = game.valid_cards_indices(&self.hand.cards, trick);
        if allowed.is_empty() {
            card::Play::Pass
        } else {
            //let mut rng = thread_rng();
            allowed.shuffle(rng);
            let card = self.hand.cards.remove(allowed[0]);
            card::Play::Card(card)
        }
    }

    pub fn play_weighted_random(
        &mut self,
        game: &dyn Game,
        trick: &card::Trick,
        rounds: &[Round],
        rng: &mut ThreadRng,
    ) -> card::Play {
        let allowed = game.valid_cards_indices(&self.hand.cards, trick);
        if allowed.is_empty() || self.weights.is_none() {
            card::Play::Pass
        } else {
            let iround = rounds.len();
            let turn = match trick.plays.len() {
                0 => Turn::First,
                3 => Turn::Last,
                _ => Turn::Other,
            };
            let weights = self.weights.unwrap();
            let weights2: Vec<i64> = if let Some(lead) = trick.lead {
                allowed
                    .iter()
                    .map(|&index| {
                        let card = &self.hand.cards[index];
                        let rel = match lead.rank.cmp(&card.rank) {
                            Ordering::Greater => 1,
                            Ordering::Less => -1,
                            Ordering::Equal => 0,
                        };
                        //let key = (iround, lead.suit, lead.rank, turn, *card);
                        let key = (iround, lead.suit, turn, card.suit, rel);
                        *weights.get(&key).unwrap()
                    })
                    .collect()
            } else {
                //For testing
                //let weights: Vec<usize> = allowed.iter().map(|_| 1).collect();
                //let weights: Vec<usize> = allowed.iter().map(|&index| {
                //	let card = &self.hand.cards[index];
                //	if card.rank > card::Rank::Ten {
                //		3
                //	} else if card.rank > card::Rank::Five {
                //		2
                //	} else {
                //		1
                //	}
                //}).collect();
                allowed
                    .iter()
                    .map(|&_index| 1) //self.weights.weights.get(&self.hand.cards[index]).unwrap())
                    .collect()
            };
            let dist = WeightedIndex::new(&weights2).unwrap();
            //let mut rng = thread_rng();
            let index = allowed[dist.sample(rng)];
            //allowed.shuffle(&mut rng);
            let card = self.hand.cards.remove(index);
            card::Play::Card(card)
        }
    }

    pub fn play_weighted(
        &mut self,
        game: &dyn Game,
        trick: &card::Trick,
        rounds: &[Round],
        _rng: &mut ThreadRng,
    ) -> card::Play {
        let allowed = game.valid_cards_indices(&self.hand.cards, trick);
        if allowed.is_empty() || self.weights.is_none() {
            card::Play::Pass
        } else {
            let iround = rounds.len();
            let turn = match trick.plays.len() {
                0 => Turn::First,
                3 => Turn::Last,
                _ => Turn::Other,
            };
            let weights = self.weights.unwrap();
            let weights2: Vec<i64> = if let Some(lead) = trick.lead {
                allowed
                    .iter()
                    .map(|&index| {
                        let card = &self.hand.cards[index];
                        let rel = match lead.rank.cmp(&card.rank) {
                            Ordering::Greater => 1,
                            Ordering::Less => -1,
                            Ordering::Equal => 0,
                        };
                        //let key = (iround, lead.suit, lead.rank, turn, *card);
                        let key = (iround, lead.suit, turn, card.suit, rel);
                        *weights.get(&key).unwrap()
                    })
                    .collect()
            } else {
                //For testing
                //let weights: Vec<usize> = allowed.iter().map(|_| 1).collect();
                //let weights: Vec<usize> = allowed.iter().map(|&index| {
                //	let card = &self.hand.cards[index];
                //	if card.rank > card::Rank::Ten {
                //		3
                //	} else if card.rank > card::Rank::Five {
                //		2
                //	} else {
                //		1
                //	}
                //}).collect();
                allowed
                    .iter()
                    .map(|&_index| 1) //self.weights.weights.get(&self.hand.cards[index]).unwrap())
                    .collect()
            };

            let mut best = (0, 0);
            for (&weight, &index) in weights2.iter().zip(&allowed) {
                if weight > best.0 {
                    best = (weight, index);
                }
            }
            let card = self.hand.cards.remove(best.1);
            card::Play::Card(card)
        }
    }
}

impl fmt::Display for Player<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ({}): {}", self.name, self.id, self.hand)
    }
}
