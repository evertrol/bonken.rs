use bonken::card::{Card, Play, Rank, Suit};
use bonken::game::duck::Duck;
use bonken::game::hearts::Hearts;
use bonken::game::jacksandkings::JacksAndKings;
use bonken::game::kingofhearts::KingOfHearts;
use bonken::game::last::Last;
use bonken::game::notrump::NoTrump;
use bonken::game::queens::Queens;
use bonken::game::seventhirteen::SevenThirteen;
use bonken::game::trump::Trump;
use bonken::game::Game;
use bonken::player::{Method, Player, Turn};
use bonken::table::Table;
use bonken::two_to_ace;
use bonken::weights::{
    normalize_weights, normalize_weights2, weights2_to_jsonweights2, JSONWeights, Weights2,
};
use rand::prelude::*;
use std::cmp::Ordering;
use std::collections::hash_map::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use structopt::clap::arg_enum;
use structopt::StructOpt;

arg_enum! {
    #[derive(Debug)]
    enum GameName {
        NoTrump,
        TrumpOfSpades,
        TrumpOfHearts,
        TrumpOfDiamonds,
        TrumpOfClubs,
        Hearts,
        Trump, // random trump game
        KingOfHearts,
        LastTrick,
        Last,
        SevenThirteen,
        KingsJacks,
        Queens,
        Duck,
    }
}

impl GameName {
    fn lower(&self) -> String {
        format!("{}", self).to_lowercase()
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "BonkenBot", about = "Simulate a Bonken game.")]
struct Opt {
    #[structopt(possible_values=&GameName::variants(), case_insensitive=true)]
    game: GameName,

    #[structopt(short, long)]
    nsim: Option<usize>,

    #[structopt(short, long, parse(from_os_str))]
    output: Option<PathBuf>,

    #[structopt(long, parse(from_os_str))]
    output_games: Option<PathBuf>,

    #[structopt(long)]
    output_nth_game: Option<usize>,

    #[structopt(long)]
    recalc_weights: Option<usize>,
}

fn write_table_to_file(file: &mut File, table: &Table, igame: usize) {
    let mut games: Vec<String> = vec![];
    for round in table.rounds.iter() {
        let winner = if let Some(winner) = &round.winner {
            winner.id
        } else {
            0
        };
        let plays: Vec<String> = round
            .plays
            .iter()
            .map(|(player, play)| {
                let card = match play {
                    Play::NA => "null".to_string(),
                    Play::Pass => "pass".to_string(),
                    Play::Card(card) => card.to_string(),
                    _ => "".to_string(),
                };
                format!("{}:{}", player.id, card)
            })
            .collect();

        let points: Vec<String> = round
            .scores
            .iter()
            .map(|(id, i)| format!("{}:{}", id, i))
            .collect();
        games.push(format!(
            "{{trick:[{}],points:[{}],win:{}}}",
            plays.join(","),
            points.join(","),
            winner
        ));
    }
    let scores: Vec<String> = table
        .scores
        .iter()
        .map(|(id, i)| format!("{}:{}", id, i))
        .collect();
    write!(
        file,
        "iteration={};game={};scores={{{}}};rounds=",
        igame,
        table.game.name(),
        scores.join(",")
    )
    .expect("can't write to output file");
    writeln!(file, "[{}]", games.join(",")).expect("can't write to output file");
}

fn init_jsonweights(weights: &mut JSONWeights, value: i64) {
    for iround in 0..13 {
        for leadsuit in &[Suit::Spades, Suit::Hearts, Suit::Diamonds, Suit::Clubs] {
            for leadrank in &two_to_ace!() {
                for turn in &[Turn::First, Turn::Last, Turn::Other] {
                    for suit in &[Suit::Spades, Suit::Hearts, Suit::Diamonds, Suit::Clubs] {
                        for rank in &two_to_ace!() {
                            let card = Card {
                                suit: *suit,
                                rank: *rank,
                            };

                            weights
                                .entry(iround)
                                .or_insert_with(HashMap::new)
                                .entry(*leadsuit)
                                .or_insert_with(HashMap::new)
                                .entry(*leadrank)
                                .or_insert_with(HashMap::new)
                                .entry(*turn)
                                .or_insert_with(HashMap::new)
                                .insert(card, value);
                        }
                    }
                }
            }
        }
    }
}

fn init_weights(weights: &mut Weights2, value: i64) {
    for iround in 0..13 {
        for leadsuit in &[Suit::Spades, Suit::Hearts, Suit::Diamonds, Suit::Clubs] {
            for turn in &[Turn::First, Turn::Last, Turn::Other] {
                for suit in &[Suit::Spades, Suit::Hearts, Suit::Diamonds, Suit::Clubs] {
                    for i in -1..=1 {
                        let key = (iround, *leadsuit, *turn, *suit, i);
                        weights.insert(key, value);
                    }
                }
            }
        }
    }
}

fn main() {
    let opt = Opt::from_args();
    log::debug!("{:?}", opt);

    let ngames = opt.nsim.unwrap_or(1);
    let name = opt.game.lower();

    let mut outfile: Option<File> = if let Some(filename) = opt.output_games {
        Some(File::create(filename).expect("can't open output file"))
    } else {
        None
    };
    let output_nth_game: usize = opt.output_nth_game.unwrap_or(1);
    let nrecalc: usize = opt.recalc_weights.unwrap_or(ngames);

    let mut weights: JSONWeights = HashMap::new();
    let mut weights2: Vec<Weights2> = vec![];
    let mut weights_p: Vec<Weights2> = vec![];
    init_jsonweights(&mut weights, 0);
	for i in 0..4 {
		weights2.push(HashMap::new());
		init_weights(&mut weights2[i], 0);
		weights_p.push(HashMap::new());
		init_weights(&mut weights_p[i], 100);
	}

    // Players in order; dealing for the first round will start at the
    // first player.
    //let mut tables: Vec<Table> = vec![];
    let mut suits = vec![Suit::Spades, Suit::Hearts, Suit::Diamonds, Suit::Clubs];
    let mut rng = thread_rng();

    for _ in 0..nrecalc {
        {
            let game = match opt.game {
                GameName::NoTrump => Box::new(NoTrump::new()) as Box<dyn Game>,
                GameName::Trump => {
                    suits.shuffle(&mut rng);
                    Box::new(Trump::new(suits[0])) as Box<dyn Game>
                }
                GameName::TrumpOfSpades => Box::new(Trump::new(Suit::Spades)) as Box<dyn Game>,
                GameName::TrumpOfHearts => Box::new(Trump::new(Suit::Hearts)) as Box<dyn Game>,
                GameName::TrumpOfDiamonds => Box::new(Trump::new(Suit::Diamonds)) as Box<dyn Game>,
                GameName::TrumpOfClubs => Box::new(Trump::new(Suit::Clubs)) as Box<dyn Game>,

                GameName::Duck => Box::new(Duck::new()) as Box<dyn Game>,
                GameName::SevenThirteen => Box::new(SevenThirteen::new()) as Box<dyn Game>,
                GameName::Last | GameName::LastTrick => Box::new(Last::new()) as Box<dyn Game>,
                GameName::Queens => Box::new(Queens::new()) as Box<dyn Game>,
                GameName::KingsJacks => Box::new(JacksAndKings::new()) as Box<dyn Game>,
                GameName::KingOfHearts => Box::new(KingOfHearts::new()) as Box<dyn Game>,
                GameName::Hearts => Box::new(Hearts::new()) as Box<dyn Game>,
            };
            let players = vec![
                Player::new(&"Bot 1"),
                Player::new(&"Bot 2"),
                Player::new(&"Bot 3"),
                Player::new(&"Bot 4"),
            ];

            let mut table = Table::new().players(players).game(game);
            let nplayers = table.players.len();
            let maxpoints = table.game.maxpoints().abs();

            table.players[1].method = Method::WeightedRandom;
            table.players[1].set_weights(&weights_p[1]);
            table.players[2].method = Method::Weighted;
            table.players[2].set_weights(&weights_p[2]);

            for igame in 0..ngames {
                log::info!("=== Game {} ===", igame + 1);
                table.play(&mut rng);

                if let Some(ref mut file) = outfile {
                    if igame % output_nth_game == 0 {
                        write_table_to_file(file, &table, igame + 1);
                    }
                }

				for iplayer in 0..4 {
                for (iround, round) in table.rounds.iter().enumerate() {
                    if let Some((_, Play::Card(lead))) = round.lead() {
                        for (i, (player, play)) in round.plays.iter().enumerate() {
                            if let Play::Card(card) = play {
                                let score = round.scores.get(&player.id).unwrap();
                                let turn = if i == 0 {
                                    Turn::First
                                } else if i == nplayers - 1 {
                                    Turn::Last
                                } else {
                                    Turn::Other
                                };
                                let rel = match lead.rank.cmp(&card.rank) {
                                    Ordering::Greater => 1,
                                    Ordering::Less => -1,
                                    Ordering::Equal => 0,
                                };
                                let key = (iround, lead.suit, turn, card.suit, rel);
                                if let Some(weight) = weights2[iplayer].get_mut(&key) {
                                    *weight += match score.cmp(&0) {
										Ordering::Greater => 1,
										Ordering::Less => -1,
										Ordering::Equal => 0,
									};
									//*weight += (score * 100) / maxpoints;
                                }
                                if let Some(value1) = weights.get_mut(&iround) {
                                    if let Some(value2) = value1.get_mut(&lead.suit) {
                                        if let Some(value3) = value2.get_mut(&lead.rank) {
                                            if let Some(value4) = value3.get_mut(&turn) {
                                                if let Some(weight) = value4.get_mut(card) {
                                                    *weight += (score * 100) / maxpoints;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
						}
                        }
                    }
                }
            }
        }

        normalize_weights(&mut weights);
		for weights22 in weights2.iter_mut() {
			normalize_weights2(weights22);
		}

        //weights_p1 = weights2.clone();
		for (i, weights_pp) in weights_p.iter_mut().enumerate() {
			for (key, value) in weights2[i].iter() {
				weights_pp.entry(*key).and_modify(|e| *e = *value);
			}
        }
    }

    normalize_weights(&mut weights);
	for weights22 in weights2.iter_mut() {
		normalize_weights2(weights22);
	}


    let filename = opt
        .output
        .unwrap_or_else(|| PathBuf::from(format!("{}-weights.json", name)));
    let json = serde_json::to_string(&weights).expect("JSON serialization failed");
    let mut file = File::create(filename).expect("can't open output file");
    writeln!(file, "{}", json).expect("can't write to output file");

	for i in 0..4 {
		let filename = PathBuf::from(format!("{}-weights2-{}.json", name, i));
		let json_weights2 = weights2_to_jsonweights2(&weights2[i]);
		let json = serde_json::to_string(&json_weights2).expect("JSON serialization failed");
		let mut file = File::create(filename).expect("can't open output file");
		writeln!(file, "{}", json).expect("can't write to output file");
	}
}
