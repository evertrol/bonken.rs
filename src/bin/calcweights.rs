use bonken::card::{Card, Play, Rank, Suit};
use bonken::player::PlayerId;
use bonken::two_to_ace;
use serde::Serialize;
use std::cmp::Ordering;
use std::collections::hash_map::HashMap;
use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::io::{self, BufRead};
use std::path::PathBuf;
use structopt::StructOpt;

pub type Scores = HashMap<PlayerId, i64>;
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Serialize)]
pub enum Turn {
    First,
    Last,
    Other,
}
pub type SingleWeight = HashMap<(usize, Turn, Rank, Suit), i64>;
pub type Weights = HashMap<PlayerId, SingleWeight>;

pub type Map = HashMap<String, HashMap<usize, HashMap<Turn, HashMap<Card, i64>>>>;

impl Default for Turn {
    fn default() -> Self {
        Turn::Other
    }
}

impl fmt::Display for Turn {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Turn::First => write!(f, "first"),
            Turn::Last => write!(f, "last"),
            Turn::Other => write!(f, "other"),
        }
    }
}

#[derive(Debug, Default)]
struct Round {
    pub winner: Option<PlayerId>,
    pub lead: Option<Card>,
    pub plays: Vec<(PlayerId, Play)>,
    pub points: HashMap<PlayerId, i64>,
}

impl fmt::Display for Round {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let play_strings: Vec<String> = self
            .plays
            .iter()
            .map(|play| format!("{}: {:}", play.0, play.1))
            .collect();
        let point_strings: Vec<String> = self
            .points
            .iter()
            .map(|(id, score)| format!("{}: {}", id, score))
            .collect();
        if let Some(winner) = &self.winner {
            write!(
                f,
                "{}; {} wins. Points: {}",
                play_strings.join(", "),
                winner,
                point_strings.join(", ")
            )
        } else {
            write!(
                f,
                "{}; no one wins. Points: {}",
                play_strings.join(", "),
                point_strings.join(", ")
            )
        }
    }
}

fn parse_scores(string: &str) -> HashMap<PlayerId, i64> {
    let string = string.trim_start_matches('[').trim_end_matches(']');

    let mut scores: HashMap<PlayerId, i64> = HashMap::new();
    for part in string.split(',') {
        let kv: Vec<&str> = part.split(':').collect();
        let id: PlayerId = kv[0]
            .parse()
            .expect("expected positive number for player id");
        let score: i64 = kv[1].parse().expect("expected integer score");
        scores.insert(id, score);
    }
    //println!("{:?}", scores);
    scores
}

fn parse_round_string(string: &str) -> Round {
    let mut round: Round = Round::default();

    let parts: Vec<&str> = string.split("],").collect();

    let kv: Vec<&str> = parts[0].split(':').collect();
    if kv[0] != "trick" {
        panic!("expected key 'trick'");
    }
    let trick = kv[1..].join(":");
    for play in trick.trim_start_matches('[').split(',') {
        let kv: Vec<&str> = play.split(':').collect();
        let id: PlayerId = kv[0]
            .parse()
            .expect("expected positive number for player id");
        let card: Card = kv[1].parse().expect("expected suit & card");
        if round.lead.is_none() {
            round.lead = Some(card);
        }
        let play = Play::Card(card);
        round.plays.push((id, play));
    }

    let kv: Vec<&str> = parts[1].split(':').collect();
    if kv[0] != "points" {
        panic!("expected key 'points'");
    }
    let points = kv[1..].join(":");
    for point in points.trim_start_matches('[').split(',') {
        let kv: Vec<&str> = point.split(':').collect();
        let id: PlayerId = kv[0]
            .parse()
            .expect("expected positive number for player id");
        let score: i64 = kv[1].parse().expect("expected integer points");
        round.points.insert(id, score);
    }

    let kv: Vec<&str> = parts[2].split(':').collect();
    if kv[0] != "win" {
        panic!("expected key 'win'");
    }
    round.winner = kv[1].parse().ok();

    round
}

fn parse_roundsstring(string: &str) -> Vec<Round> {
    let string = string.trim_start_matches("[{").trim_end_matches("}]");
    let rounds: Vec<Round> = string
        .split("},{")
        .map(|part| parse_round_string(part))
        .collect();

    rounds
}

fn parse_string(string: &str) -> (Vec<Round>, Scores, String) {
    let parts: Vec<&str> = string.split(';').collect();
    let mut rounds: Vec<Round> = vec![];
    let mut scores: HashMap<PlayerId, i64> = HashMap::new();
    let mut name = "".to_string();
    for part in parts.iter() {
        let kv: Vec<&str> = part.split('=').collect();
        if kv[0] == "scores" {
            scores = parse_scores(&kv[1]);
        }
        if kv[0] == "rounds" {
            rounds = parse_roundsstring(&kv[1]);
        }
        if kv[0] == "game" {
            name = kv[1].to_string();
        }
    }

    (rounds, scores, name)
}

fn parse_rounds(rounds: &[Round], scores: &HashMap<PlayerId, i64>, weights: &mut Weights) {
    let maxid = scores.len() - 1;
    for (iround, round) in rounds.iter().enumerate() {
        for (i, (id, play)) in round.plays.iter().enumerate() {
            let single = weights.entry(*id);
            let points = *round.points.get(id).unwrap_or(&0);
            let turn = if i == 0 {
                Turn::First
            } else if i == maxid {
                Turn::Last
            } else {
                Turn::Other
            };
            if let Play::Card(card) = play {
                let key = (iround, turn, card.rank, card.suit);
                single.and_modify(|entry| {
                    let weights = entry.entry(key).or_insert(0);
                    *weights += points;
                });
            }
        }
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "BonkenBot", about = "Simulate a Bonken game.")]
struct Opt {
    #[structopt(parse(from_os_str))]
    input: PathBuf,

    #[structopt(short, long, parse(from_os_str))]
    output: Option<PathBuf>,
}

fn main() -> Result<(), io::Error> {
    let opt = Opt::from_args();
    let file = File::open(opt.input)?;
    let reader = io::BufReader::new(file);

    let mut game_name = "none".to_string();
    let mut weights = Weights::new();
    for line in reader.lines() {
        let string = line.unwrap();
        let (rounds, scores, name) = parse_string(&string);
        if weights.keys().len() == 0 {
            game_name = name;
            for id in scores.keys() {
                weights.insert(*id, SingleWeight::new());
            }
        }

        parse_rounds(&rounds, &scores, &mut weights);
    }

    game_name = game_name
        .to_lowercase()
        .split(' ')
        .collect::<Vec<&str>>()
        .join("")
        .split('-')
        .collect::<Vec<&str>>()[0]
        .to_string();
    println!("{}", game_name);
    for (id, weight) in weights.iter() {
        println!("{}: {}", id, weight.len());
    }

    let mut map: Map = HashMap::new();

    let mut max = 0;
    let mut min = 0;
    for iround in 0..13 {
        for turn in &[Turn::First, Turn::Last, Turn::Other] {
            for suit in &[Suit::Spades, Suit::Hearts, Suit::Diamonds, Suit::Clubs] {
                for rank in two_to_ace!().iter() {
                    let key = (iround, *turn, *rank, *suit);
                    let mut weight = 0;
                    for id in weights.keys() {
                        weight += weights.get(id).unwrap().get(&key).unwrap_or(&0);
                    }
                    weight /= weights.len() as i64;
                    if weight < min {
                        min = weight;
                    }
                    if weight > max {
                        max = weight;
                    }
                    map.entry(game_name.clone())
                        .or_insert_with(HashMap::new)
                        .entry(iround)
                        .or_insert_with(HashMap::new)
                        .entry(*turn)
                        .or_insert_with(HashMap::new)
                        .insert(
                            Card {
                                suit: *suit,
                                rank: *rank,
                            },
                            weight,
                        );
                }
            }
        }
    }
    let min = match min.cmp(&0) {
        Ordering::Greater => (min as f64).ln(),
        Ordering::Less => -(-min as f64).ln(),
        Ordering::Equal => 0.0,
    };
    let max = match max.cmp(&0) {
        Ordering::Greater => (max as f64).ln(),
        Ordering::Less => -(-max as f64).ln(),
        Ordering::Equal => 0.0,
    };
    let delta = max - min;
    for iround in 0..13 {
        for turn in &[Turn::First, Turn::Last, Turn::Other] {
            for suit in &[Suit::Spades, Suit::Hearts, Suit::Diamonds, Suit::Clubs] {
                for rank in two_to_ace!().iter() {
                    let weight = *map
                        .get(&game_name)
                        .unwrap()
                        .get(&iround)
                        .unwrap()
                        .get(turn)
                        .unwrap()
                        .get(&Card {
                            suit: *suit,
                            rank: *rank,
                        })
                        .unwrap();
                    let weight = match weight.cmp(&0) {
                        Ordering::Greater => (weight as f64).ln(),
                        Ordering::Less => -(-weight as f64).ln(),
                        Ordering::Equal => 0.0,
                    } - min;
                    let weight = (100.0 * (weight as f64 / delta as f64)) as i64;
                    map.entry(game_name.clone())
                        .or_insert_with(HashMap::new)
                        .entry(iround)
                        .or_insert_with(HashMap::new)
                        .entry(*turn)
                        .or_insert_with(HashMap::new)
                        .insert(
                            Card {
                                suit: *suit,
                                rank: *rank,
                            },
                            weight,
                        );
                }
            }
        }
    }

    let filename = opt.output.unwrap_or_else(|| PathBuf::from("weights.json"));
    let json = serde_json::to_string(&map).expect("JSON serialization failed");
    let mut file = File::create(filename).expect("can't open output file");
    writeln!(file, "{}", json).expect("can't write to output file");

    Ok(())
}
