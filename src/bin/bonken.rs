use bonken::card::{Play, Suit};
use bonken::game::duck::Duck;
use bonken::game::hearts::Hearts;
use bonken::game::jacksandkings::JacksAndKings;
use bonken::game::kingofhearts::KingOfHearts;
use bonken::game::last::Last;
use bonken::game::notrump::NoTrump;
use bonken::game::queens::Queens;
use bonken::game::seventhirteen::SevenThirteen;
use bonken::game::trump::Trump;
use bonken::game::Game;
use bonken::player::{Method, Player};
use bonken::table::Table;
use bonken::weights::{read_weights, read_weights2};
use rand::prelude::*;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use structopt::clap::arg_enum;
use structopt::StructOpt;

arg_enum! {
    #[derive(Debug)]
    enum GameName {
        NoTrump,
        TrumpOfSpades,
        TrumpOfHearts,
        TrumpOfDiamonds,
        TrumpOfClubs,
        Hearts,
        Trump, // random trump game
        KingOfHearts,
        LastTrick,
        Last,
        SevenThirteen,
        KingsJacks,
        Queens,
        Duck,
    }
}

#[derive(Debug, StructOpt)]
#[structopt(name = "BonkenBot", about = "Simulate a Bonken game.")]
struct Opt {
    #[structopt(possible_values=&GameName::variants(), case_insensitive=true)]
    game: GameName,

    #[structopt(short, long)]
    nsim: Option<usize>,

    #[structopt(short, long, parse(from_os_str))]
    output: Option<PathBuf>,

    #[structopt(long)]
    output_nth_game: Option<usize>,
}

fn write_table_to_file(file: &mut File, table: &Table, igame: usize) {
    let mut games: Vec<String> = vec![];
    for round in table.rounds.iter() {
        let winner = if let Some(winner) = &round.winner {
            winner.id
        } else {
            0
        };
        let plays: Vec<String> = round
            .plays
            .iter()
            .map(|(player, play)| {
                let card = match play {
                    Play::NA => "null".to_string(),
                    Play::Pass => "pass".to_string(),
                    Play::Card(card) => card.to_string(),
                    _ => "".to_string(),
                };
                format!("{}:{}", player.id, card)
            })
            .collect();

        let points: Vec<String> = round
            .scores
            .iter()
            .map(|(id, i)| format!("{}:{}", id, i))
            .collect();
        games.push(format!(
            "{{trick:[{}],points:[{}],win:{}}}",
            plays.join(","),
            points.join(","),
            winner
        ));
    }
    let scores: Vec<String> = table
        .scores
        .iter()
        .map(|(id, i)| format!("{}:{}", id, i))
        .collect();
    write!(
        file,
        "iteration={};game={};scores={{{}}};rounds=",
        igame,
        table.game.name(),
        scores.join(",")
    )
    .expect("can't write to output file");
    writeln!(file, "[{}]", games.join(",")).expect("can't write to output file");
}

fn main() {
    env_logger::init();

    let opt = Opt::from_args();
    log::debug!("{:?}", opt);

    let ngames = opt.nsim.unwrap_or(1);
    let filename = opt.output.unwrap_or_else(|| PathBuf::from("bonken.dat"));

    let mut file = File::create(filename).expect("can't open output file");
    let output_nth_game: usize = opt.output_nth_game.unwrap_or(1);

    // Players in order; dealing for the first round will start at the
    // first player.
    //let mut tables: Vec<Table> = vec![];
    let mut suits = vec![Suit::Spades, Suit::Hearts, Suit::Diamonds, Suit::Clubs];
    let mut rng = thread_rng();
    let game = match opt.game {
        GameName::NoTrump => Box::new(NoTrump::new()) as Box<dyn Game>,
        GameName::Trump => {
            suits.shuffle(&mut rng);
            Box::new(Trump::new(suits[0])) as Box<dyn Game>
        }
        GameName::TrumpOfSpades => Box::new(Trump::new(Suit::Spades)) as Box<dyn Game>,
        GameName::TrumpOfHearts => Box::new(Trump::new(Suit::Hearts)) as Box<dyn Game>,
        GameName::TrumpOfDiamonds => Box::new(Trump::new(Suit::Diamonds)) as Box<dyn Game>,
        GameName::TrumpOfClubs => Box::new(Trump::new(Suit::Clubs)) as Box<dyn Game>,

        GameName::Duck => Box::new(Duck::new()) as Box<dyn Game>,
        GameName::SevenThirteen => Box::new(SevenThirteen::new()) as Box<dyn Game>,
        GameName::Last | GameName::LastTrick => Box::new(Last::new()) as Box<dyn Game>,
        GameName::Queens => Box::new(Queens::new()) as Box<dyn Game>,
        GameName::KingsJacks => Box::new(JacksAndKings::new()) as Box<dyn Game>,
        GameName::KingOfHearts => Box::new(KingOfHearts::new()) as Box<dyn Game>,
        GameName::Hearts => Box::new(Hearts::new()) as Box<dyn Game>,
    };
    let _weights = read_weights(&game.short_name()).expect("can't read weights");
    let weights2 = read_weights2(&game.short_name()).expect("can't read weights");
    //players[1].set_weights(Box::new(&weights1));
    let mut players = vec![
        Player::new("Bot 1"),
        Player::new("Bot 2"),
        Player::new("Bot 3"),
        Player::new("Bot 4"),
    ];
    players[1].method = Method::WeightedRandom;
    players[1].set_weights(&weights2);
    players[2].method = Method::Weighted;
    players[2].set_weights(&weights2);
    let mut table = Table::new().players(players).game(game);

    for igame in 0..ngames {
        log::info!("=== Game {} ===", igame + 1);
        table.play(&mut rng);
        if igame % output_nth_game == 0 {
            write_table_to_file(&mut file, &table, igame + 1);
        }
    }
}
