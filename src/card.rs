use itertools::iproduct;
use rand::prelude::*;
use rand::rngs::ThreadRng;
use serde::de;
use serde::ser::{self, SerializeMap};
use serde::{Deserialize, Serialize};
use std::collections::hash_map::HashMap;
use std::error::Error;
use std::fmt;
use std::str::FromStr;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum ParseError {
    InvalidSuit,
    InvalidRank,
}

impl ParseError {
    pub fn as_str(&self) -> &'static str {
        match *self {
            ParseError::InvalidSuit => "the suit is unknown",
            ParseError::InvalidRank => "the rank is unknown",
        }
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

impl Error for ParseError {
    fn description(&self) -> &str {
        self.as_str()
    }
}

#[derive(
    Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, EnumIter, Hash, Serialize, Deserialize,
)]
pub enum Suit {
    Spades,
    Hearts,
    Diamonds,
    Clubs,
}

impl Default for Suit {
    fn default() -> Self {
        Suit::Spades
    }
}

impl fmt::Display for Suit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Suit::Spades => write!(f, "♠"),
            Suit::Hearts => write!(f, "♥"),
            Suit::Diamonds => write!(f, "♦"),
            Suit::Clubs => write!(f, "♣"),
        }
    }
}

impl FromStr for Suit {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_uppercase().as_ref() {
            "SPADES" | "SPADE" | "♠" => Ok(Suit::Spades),
            "HEARTS" | "HEART" | "♥" => Ok(Suit::Hearts),
            "DIAMONDS" | "DIAMOND" | "♦" => Ok(Suit::Diamonds),
            "CLUBS" | "CLUB" | "♣" => Ok(Suit::Clubs),
            _ => Err(ParseError::InvalidSuit),
        }
    }
}

#[derive(
    Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, EnumIter, Hash, Serialize, Deserialize,
)]
pub enum Rank {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

#[macro_export]
macro_rules! ace_to_king {
    () => {
        [
            Rank::Ace,
            Rank::Two,
            Rank::Three,
            Rank::Four,
            Rank::Five,
            Rank::Six,
            Rank::Seven,
            Rank::Eight,
            Rank::Nine,
            Rank::Ten,
            Rank::Jack,
            Rank::Queen,
            Rank::King,
        ]
    };
}

#[macro_export]
macro_rules! two_to_ace {
    () => {
        [
            Rank::Two,
            Rank::Three,
            Rank::Four,
            Rank::Five,
            Rank::Six,
            Rank::Seven,
            Rank::Eight,
            Rank::Nine,
            Rank::Ten,
            Rank::Jack,
            Rank::Queen,
            Rank::King,
            Rank::Ace,
        ]
    };
}

#[macro_export]
macro_rules! card {
    ($rank:ident, $suit:ident) => {
        Card {
            suit: Suit::$suit,
            rank: Rank::$rank,
        }
    };
}

impl Default for Rank {
    fn default() -> Self {
        Rank::Ace
    }
}

impl fmt::Display for Rank {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Rank::Jack => write!(f, "J"),
            Rank::Queen => write!(f, "Q"),
            Rank::King => write!(f, "K"),
            Rank::Ace => write!(f, "A"),
            //Rank::Number(n) => write!(f, "{}", n),
            Rank::Two => write!(f, "2"),
            Rank::Three => write!(f, "3"),
            Rank::Four => write!(f, "4"),
            Rank::Five => write!(f, "5"),
            Rank::Six => write!(f, "6"),
            Rank::Seven => write!(f, "7"),
            Rank::Eight => write!(f, "8"),
            Rank::Nine => write!(f, "9"),
            Rank::Ten => write!(f, "10"),
        }
    }
}

impl FromStr for Rank {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_uppercase().as_ref() {
            "A" => Ok(Rank::Ace),
            "K" => Ok(Rank::King),
            "Q" => Ok(Rank::Queen),
            "J" => Ok(Rank::Jack),
            "10" | "TEN" => Ok(Rank::Ten),
            "9" | "NINE" => Ok(Rank::Nine),
            "8" | "EIGHT" => Ok(Rank::Eight),
            "7" | "SEVEN" => Ok(Rank::Seven),
            "6" | "SIX" => Ok(Rank::Six),
            "5" | "FIVE" => Ok(Rank::Five),
            "4" | "FOUR" => Ok(Rank::Four),
            "3" | "THREE" => Ok(Rank::Three),
            "2" | "TWO" => Ok(Rank::Two),
            _ => Err(ParseError::InvalidRank),
        }
    }
}

#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Card {
    pub suit: Suit,
    pub rank: Rank,
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}", self.suit, self.rank)
    }
}

impl FromStr for Card {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars();
        let suit: Suit = format!("{}", chars.next().unwrap()).parse()?;
        let rank: Rank = chars.as_str().parse()?;
        Ok(Card { suit, rank })
    }
}

impl ser::Serialize for Card {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: ser::Serializer,
    {
        serializer.serialize_str(format!("{}", self).as_ref())
    }
}

struct CardVisitor;

impl<'de> de::Visitor<'de> for CardVisitor {
    type Value = Card;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a string containing a suit and a rank")
    }

    fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        let card: Result<Card, ParseError> = s.parse();
        if card.is_ok() {
            Ok(card.unwrap())
        } else {
            Err(de::Error::invalid_value(de::Unexpected::Str(s), &self))
        }
    }
}

impl<'de> Deserialize<'de> for Card {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        deserializer.deserialize_str(CardVisitor)
    }
}
//	}

// Decide: change to a tuple struct
// Then, .cards would become .0
#[derive(Debug, Clone, Serialize)]
pub struct Deck {
    pub cards: Vec<Card>,
}

impl Deck {
    pub fn sort(&mut self) {
        self.cards.sort();
    }

    pub fn shuffle(&mut self, rng: &mut ThreadRng) {
        //let mut rng = thread_rng();
        self.cards.shuffle(rng);
    }

    pub fn len(&self) -> usize {
        self.cards.len()
    }

    pub fn empty() -> Deck {
        Deck { cards: vec![] }
    }

    pub fn is_empty(&self) -> bool {
        self.cards.is_empty()
    }
}

impl Default for Deck {
    fn default() -> Deck {
        let mut cards = vec![]; //Card { suit: Suit::Spades, rank: Rank::Ace }; 52];
        for (suit, rank) in iproduct!(Suit::iter(), Rank::iter()) {
            cards.push(Card { suit, rank });
        }
        Deck { cards }
    }
}

impl fmt::Display for Deck {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let strings: Vec<String> = self.cards.iter().map(|card| format!("{}", card)).collect();
        write!(f, "[{}]", strings.join(", "))
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Play {
    NA,
    Pass,
    Card(Card),
    Cards(Vec<Card>),
}

impl Default for Play {
    fn default() -> Self {
        Play::Pass
    }
}

impl fmt::Display for Play {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Play::Pass => write!(f, ""),
            Play::NA => write!(f, "pass"),
            Play::Card(card) => write!(f, "{}", card),
            Play::Cards(cards) => {
                let strings: Vec<String> = cards.iter().map(|card| format!("{}", card)).collect();
                write!(f, "[{}]", strings.join(", "))
            } //			Play::NA => write!(f, ""),
        }
    }
}

impl ser::Serialize for Play {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: ser::Serializer,
    {
        let mut play: HashMap<&str, String> = HashMap::new();
        play.insert("play", self.to_string());
        let mut map = serializer.serialize_map(Some(play.len()))?;
        for (k, v) in &play {
            map.serialize_entry(&k, &v)?;
        }
        map.end()
    }
}

#[derive(Debug, Clone, Default)]
pub struct Trick {
    pub plays: Vec<Play>,
    pub lead: Option<Card>,
}

impl Trick {
    pub fn add(&mut self, play: Play) {
        self.plays.push(play);
    }
}
