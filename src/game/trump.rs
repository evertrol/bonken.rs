use crate::card::{Card, Deck, Play, Rank, Suit, Trick};
use crate::game::Game;
use crate::two_to_ace;

#[derive(Debug, Clone, Default)]
pub struct Trump {
    name: String,
    short_name: String,
    pub trump: Suit,
    pub ranking: [Rank; 13],
}

impl Trump {
    pub fn new(suit: Suit) -> Self {
        let short_name = match suit {
            Suit::Spades => "spades",
            Suit::Hearts => "hearts",
            Suit::Diamonds => "diamonds",
            Suit::Clubs => "clubs",
        };
        Trump {
            name: format!("Trump of {}", suit),
            short_name: format!("trumpof{}", short_name).to_lowercase(),
            trump: suit,
            ranking: two_to_ace!(),
        }
    }
}

impl Game for Trump {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn short_name(&self) -> String {
        self.short_name.clone()
    }

    fn start(&self) {
        println!("Starting game \"trump of {}\"", self.trump);
    }

    fn maxpoints(&self) -> i16 {
        260
    }

    fn deck(&self) -> Deck {
        Deck::default()
    }

    fn calc_winner(&self, trick: &Trick) -> Option<usize> {
        let mut best: Option<Card> = None;
        let mut ibest: usize = 0;
        for (i, play) in trick.plays.iter().enumerate() {
            // Didn't pass (or was forced to pass)
            if let Play::Card(card) = play {
                // There is a lead card
                // Note that if there is no lead card (set by the trick),
                // the current play will never be able to take this trick
                // for this game type.
                // Alternatively, we could panic: a trick in a trump game
                // needs a lead card.
                if let Some(lead) = trick.lead {
                    // Is there already a best card?
                    if let Some(bestcard) = best {
                        if card.suit == self.trump {
                            if bestcard.suit == self.trump {
                                // Compare rank if they are both trump
                                if card.rank > bestcard.rank {
                                    log::debug!("{} > {}", card, best.unwrap());
                                    best = Some(*card);
                                    ibest = i;
                                }
                            } else {
                                // Trump wins over other suits
                                log::debug!("{} trumps {}", card, best.unwrap());
                                best = Some(*card);
                                ibest = i;
                            }
                        } else {
                            // No trump played; are we following suit?
                            if card.suit == lead.suit {
                                // Is there a best card that isn't trump?
                                if bestcard.suit != self.trump {
                                    if card.rank > bestcard.rank {
                                        best = Some(*card);
                                        ibest = i;
                                    }
                                } else {
                                    best = Some(*card);
                                    ibest = i;
                                }
                            }
                            // no else needed: card isn't trump or follows suit,
                            // thus it won't be able to take this trick
                        }
                    } else {
                        // No best card yet: this (first) card becomes the best card
                        best = Some(*card);
                        ibest = i;
                    }
                }
            }
        }
        if best.is_some() {
            Some(ibest)
        } else {
            None
        }
    }

    fn lead_from_first(&self) -> bool {
        true
    }

    fn valid_cards(&self, cards: &[Card], trick: &Trick) -> Vec<Card> {
        let mut allowed: Vec<Card> = vec![];
        if let Some(lead) = trick.lead {
            for card in cards.iter() {
                if card.suit == lead.suit {
                    allowed.push(*card);
                }
            }
            /* Can't follow suit */
            if allowed.is_empty() {
                for card in cards.iter() {
                    allowed.push(*card)
                }
            }
        } else {
            for card in cards.iter() {
                allowed.push(*card)
            }
        }
        allowed
    }

    fn valid_cards_indices(&self, cards: &[Card], trick: &Trick) -> Vec<usize> {
        let mut allowed: Vec<usize> = vec![];
        if let Some(lead) = trick.lead {
            for (i, card) in cards.iter().enumerate() {
                if card.suit == lead.suit {
                    allowed.push(i);
                }
            }
            // Can't follow suit
            if allowed.is_empty() {
                allowed = (0..cards.len()).collect();
            }
        } else {
            allowed = (0..cards.len()).collect();
        }
        allowed
    }

    fn is_valid(&self, card: &Card, cards: &[Card], trick: &Trick) -> bool {
        let allowed = self.valid_cards(cards, trick);
        allowed.iter().any(|&allowed_card| *card == allowed_card)
    }

    fn trump(&self) -> Option<Suit> {
        Some(self.trump)
    }

    fn score(&self, player: usize, winner: usize, _trick: &Trick, _round: usize) -> i16 {
        if player == winner {
            20
        } else {
            0
        }
    }
}
