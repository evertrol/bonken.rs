use crate::card::{Card, Deck, Play, Rank, Suit, Trick};
use crate::game::Game;
use crate::two_to_ace;

#[derive(Debug, Clone, Default)]
pub struct SevenThirteen {
    name: String,
    short_name: String,
    pub ranking: [Rank; 13],
}

impl SevenThirteen {
    pub fn new() -> Self {
        SevenThirteen {
            name: "No seventh & thirteenth trick".to_string(),
            short_name: "seventhirteen".to_string(),
            ranking: two_to_ace!(),
        }
    }
}

impl Game for SevenThirteen {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn short_name(&self) -> String {
        self.short_name.clone()
    }

    fn start(&self) {
        println!("Starting game \"{}\"", self.name);
    }

    fn maxpoints(&self) -> i16 {
        -100
    }

    fn deck(&self) -> Deck {
        Deck::default()
    }

    fn calc_winner(&self, trick: &Trick) -> Option<usize> {
        let mut highest: Option<Card> = None;
        let mut ihighest: usize = 0;
        for (i, play) in trick.plays.iter().enumerate() {
            if let Play::Card(card) = play {
                if let Some(lead) = trick.lead {
                    if card.suit == lead.suit {
                        if let Some(highcard) = highest {
                            if card.rank > highcard.rank {
                                log::debug!("{} > {}", card, highest.unwrap());
                                highest = Some(*card);
                                ihighest = i;
                            }
                        } else {
                            highest = Some(*card);
                            ihighest = i;
                        }
                    }
                }
            }
        }
        if highest.is_some() {
            Some(ihighest)
        } else {
            None
        }
    }

    fn lead_from_first(&self) -> bool {
        true
    }

    fn valid_cards(&self, cards: &[Card], trick: &Trick) -> Vec<Card> {
        let mut allowed: Vec<Card> = vec![];
        if let Some(lead) = trick.lead {
            for card in cards.iter() {
                if card.suit == lead.suit {
                    allowed.push(*card);
                }
            }
            /* Can't follow suit */
            if allowed.is_empty() {
                for card in cards.iter() {
                    allowed.push(*card)
                }
            }
        } else {
            for card in cards.iter() {
                allowed.push(*card)
            }
        }
        allowed
    }

    fn valid_cards_indices(&self, cards: &[Card], trick: &Trick) -> Vec<usize> {
        let mut allowed: Vec<usize> = vec![];
        if let Some(lead) = trick.lead {
            for (i, card) in cards.iter().enumerate() {
                if card.suit == lead.suit {
                    allowed.push(i);
                }
            }
            // Can't follow suit
            if allowed.is_empty() {
                allowed = (0..cards.len()).collect();
            }
        } else {
            allowed = (0..cards.len()).collect();
        }
        allowed
    }

    fn is_valid(&self, card: &Card, cards: &[Card], trick: &Trick) -> bool {
        let allowed = self.valid_cards(cards, trick);
        allowed.iter().any(|&allowed_card| *card == allowed_card)
    }

    fn trump(&self) -> Option<Suit> {
        None
    }

    fn score(&self, player: usize, winner: usize, _trick: &Trick, round: usize) -> i16 {
        // Note: rounds are zero-based numbered
        if player == winner && (round == 6 || round == 12) {
            -50
        } else {
            0
        }
    }
}
