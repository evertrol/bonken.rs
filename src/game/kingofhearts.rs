use crate::card::{Card, Deck, Play, Rank, Suit, Trick};
use crate::game::Game;
use crate::two_to_ace;

const KING: Card = Card {
    suit: Suit::Hearts,
    rank: Rank::King,
};

#[derive(Debug, Clone, Default)]
pub struct KingOfHearts {
    name: String,
    short_name: String,
    pub ranking: [Rank; 13],
}

impl KingOfHearts {
    pub fn new() -> Self {
        KingOfHearts {
            name: "King of Hearts".to_string(),
            short_name: "kingofhearts".to_string(),
            ranking: two_to_ace!(),
        }
    }
}

impl Game for KingOfHearts {
    fn name(&self) -> String {
        self.name.clone()
    }

    fn short_name(&self) -> String {
        self.short_name.clone()
    }

    fn start(&self) {
        println!("Starting game of \"King of Hearts\"");
    }

    fn maxpoints(&self) -> i16 {
        -100
    }

    fn deck(&self) -> Deck {
        Deck::default()
    }

    fn game_ends(&self, plays: &[Vec<Play>]) -> bool {
        if let Some(trick) = plays.last() {
            if trick.contains(&Play::Card(KING)) {
                return true;
            }
        }
        false
    }

    fn calc_winner(&self, trick: &Trick) -> Option<usize> {
        let mut highest: Option<Card> = None;
        let mut ihighest: usize = 0;
        for (i, play) in trick.plays.iter().enumerate() {
            if let Play::Card(card) = play {
                if let Some(lead) = trick.lead {
                    if card.suit == lead.suit {
                        if let Some(highcard) = highest {
                            if card.rank > highcard.rank {
                                log::debug!("{} > {}", card, highest.unwrap());
                                highest = Some(*card);
                                ihighest = i;
                            }
                        } else {
                            highest = Some(*card);
                            ihighest = i;
                        }
                    }
                }
            }
        }
        if highest.is_some() {
            Some(ihighest)
        } else {
            None
        }
    }

    fn lead_from_first(&self) -> bool {
        true
    }

    fn valid_cards(&self, cards: &[Card], trick: &Trick) -> Vec<Card> {
        let indices = self.valid_cards_indices(cards, trick);
        indices
            .iter()
            .map(|&index| cards[index])
            .collect::<Vec<Card>>()
    }

    fn valid_cards_indices(&self, cards: &[Card], trick: &Trick) -> Vec<usize> {
        let mut allowed: Vec<usize> = vec![];
        if let Some(lead) = trick.lead {
            for (i, card) in cards.iter().enumerate() {
                if card.suit == lead.suit {
                    allowed.push(i);
                }
            }
            // Can't follow suit
            if allowed.is_empty() {
                if let Some(index) = cards.iter().position(|&card| card == KING) {
                    // we have the king of hearts, so we have to play it
                    allowed = vec![index];
                } else {
                    allowed = (0..cards.len()).collect();
                }
            }
        } else {
            // we play the first card
            if cards.iter().any(|&card| card.suit != Suit::Hearts) {
                // we have other cards besides hearts, so we're not allowed to lead in hearts
                allowed = (0..cards.len())
                    .filter(|&i| cards[i].suit != Suit::Hearts)
                    .collect();
            } else {
                // all cards in our hands are hearts
                allowed = (0..cards.len()).collect();
            }
        }
        allowed
    }

    fn is_valid(&self, card: &Card, cards: &[Card], trick: &Trick) -> bool {
        let allowed = self.valid_cards(cards, trick);
        allowed.iter().any(|&allowed_card| *card == allowed_card)
    }

    fn score(&self, player: usize, winner: usize, trick: &Trick, _round: usize) -> i16 {
        let mut score = 0;
        if player == winner {
            for play in trick.plays.iter() {
                if let Play::Card(card) = play {
                    if *card == KING {
                        score -= 100;
                        break;
                    }
                }
            }
        }
        score
    }
}
