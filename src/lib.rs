pub mod card;
pub mod game;
pub mod player;
pub mod table;
pub mod weights;
pub mod sim;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
